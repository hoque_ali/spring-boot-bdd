# Console App

## Introduction
The app is written using Java 8, Spring Boot framework and Gradle build tool. TDD and BDD was used to develop the various components, services and APIs.

## Run Junit and JBehave (BDD) tests
* Install Java 8
* Run `./gradlew clean cleanTest build`

## Run app using jar file
• `java -jar org-ahoque-0.1.0.jar market-data.csv 1000`  // NB: App takes absolute path to market data file