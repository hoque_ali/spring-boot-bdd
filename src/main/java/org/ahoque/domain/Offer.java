package org.ahoque.domain;

import java.math.BigDecimal;

import com.opencsv.bean.CsvBindByName;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class Offer {

	@CsvBindByName(column = "lender", required = true)
	private String lender;

	@CsvBindByName(column = "rate", required = true)
	private BigDecimal rate;

	@CsvBindByName(column = "available", required = true)
	private BigDecimal available;

	public Offer() {
	};

	public Offer(String lender, BigDecimal rate, BigDecimal available) {
		this.lender = lender;
		this.rate = rate;
		this.available = available;
	}
}
