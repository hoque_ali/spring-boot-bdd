package org.ahoque.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

/**
 * Service for calculating repayment
 * 
 * @author hoque
 *
 */

@Service
public class CompoundInterestService implements InterestCalculator {

	/**
	 * Returns repayment calculated with the formula P × (1+r/12)n
	 * 
	 * @param p the principal
	 * @param r the yearly rate in decimal
	 * @param n the loan term in months
	 * @return repayment
	 * 
	 * @author hoque
	 */
	@Override
	public BigDecimal repayment(BigDecimal P, BigDecimal r, int n) throws IllegalArgumentException {

		if (P == null)
			throw new IllegalArgumentException("Invalid pricipal argument");

		if (r == null)
			throw new IllegalArgumentException("Invalid rate argument");

		if (n < 1)
			throw new IllegalArgumentException("Invalid number of months argument");

		// 1. First work out the monthly compounding rate given the rate is yearly (r/12)
		BigDecimal ans = r.divide(new BigDecimal(12), 10, RoundingMode.HALF_UP);

		// 2. Add 1 (1 + r/12)
		BigDecimal ans2 = ans.add(new BigDecimal(1));

		// 3. Raise it to the power of loan term (1 + r/12)^n
		BigDecimal ans3 = ans2.pow(n).setScale(10, RoundingMode.HALF_UP);

		// 4. Multiply my principal to get total re-payment
		return P.multiply(ans3).setScale(2, RoundingMode.HALF_UP);
	}

}
