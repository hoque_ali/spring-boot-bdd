package org.ahoque.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.ahoque.domain.Offer;
import org.ahoque.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferService {

	@Autowired
	private OfferRepository repo;

	public List<Offer> getOffers(String filePath, BigDecimal loanAmount) throws IOException {
		return repo.findAll(filePath).stream().filter(offer -> loanAmount.compareTo(offer.getAvailable()) == 0
				|| loanAmount.compareTo(offer.getAvailable()) == -1).collect(Collectors.toList());
	}

	public List<Offer> getBestRate(String filePath, BigDecimal loanAmount) throws IOException {

		// sorted by best rate
		List<Offer> offers = getOffers(filePath, loanAmount).stream()
				.sorted((o1, o2) -> o1.getRate().compareTo(o2.getRate())).collect(Collectors.toList());

		return offers;

	}

}
