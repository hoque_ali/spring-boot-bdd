package org.ahoque.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

@Service
public interface InterestCalculator {

	public BigDecimal repayment(BigDecimal p, BigDecimal r, int n);

}
