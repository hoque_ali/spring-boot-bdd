package org.ahoque.repository;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.ahoque.domain.Offer;
import org.springframework.stereotype.Repository;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

@Repository
public class CsvRepository implements OfferRepository {

	private List<Offer> offers = new ArrayList<>();

	@Override
	public List<Offer> findAll(final String filePath) throws IOException {

		try (Reader reader = Files.newBufferedReader(Paths.get(filePath));) {
			CsvToBean csvToBean = new CsvToBeanBuilder(reader).withType(Offer.class).withIgnoreLeadingWhiteSpace(true)
					.build();

			offers = csvToBean.parse();
		}

		return offers;

	}

}