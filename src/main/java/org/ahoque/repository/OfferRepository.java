package org.ahoque.repository;

import java.io.IOException;
import java.util.List;

import org.ahoque.domain.Offer;

public interface OfferRepository {

	List<Offer> findAll(String filePath) throws IOException;
}
