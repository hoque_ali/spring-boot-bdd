package org.ahoque;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.ahoque.domain.Offer;
import org.ahoque.service.InterestCalculator;
import org.ahoque.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is a console app that, given market data for loan offers, finds the best
 * rate and calculates the total compound interest and prints out the total
 * re-payment and monthly payment amounts for 36 months.
 * 
 * @author Hoque Ali
 * @version 0.1.0
 *
 */

@SpringBootApplication
public class Application implements CommandLineRunner {

	public static final int LOAN_TERM = 36;

	@Autowired
	private OfferService service;

	@Autowired
	private InterestCalculator interestCalculator;

	@Override
	public void run(String... args) throws Exception {

		if (args.length == 2) {

			try {

				String marketDataFilePath = args[0];
				int loan = Integer.parseInt(args[1]);

				// loan amount is a multiple of 100
				if (loan >= 1000 && loan <= 15000 && loan % 100 == 0) {

					BigDecimal loanAmount = new BigDecimal(loan);

					List<Offer> offers = service.getBestRate(marketDataFilePath, loanAmount);
					System.out.println("Requested amount: " + loanAmount.toPlainString());

					if (offers.isEmpty()) {
						System.out.println("Sorry, currently there are no available offers");
					} else {

						Offer offer = offers.get(0);
						BigDecimal totalRepayment = interestCalculator.repayment(loanAmount, offer.getRate(),
								LOAN_TERM);
						BigDecimal monthlyRepayment = totalRepayment.divide(new BigDecimal(LOAN_TERM), 2,
								RoundingMode.HALF_UP);

						System.out.println("Rate: "
								+ offer.getRate().multiply(new BigDecimal(100)).setScale(1, RoundingMode.HALF_UP)
								+ "%");
						System.out.println("Monthly repayment: " + "£" + monthlyRepayment);
						System.out.println("Total payment: " + "£" + totalRepayment);
					}
				} else {
					System.out.println("Please enter loan amount as multiples of 100 and between £1000 and £15000");
				}

			} catch (java.nio.file.NoSuchFileException nsfe) {

				System.out.println("Could not find market data file. Please enter a valid file path.");

			} catch (Exception e) {
				// everything else
				e.printStackTrace();
			}

		} else {
			System.out.println("Missing arguments. Please enter market data file path and loan amount");
		}

	}

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
	}

}
