package org.ahoque.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.junit.Test;

public class InterestCalculatorTest {

	@Test
	public void givenLoanAmountAndRateAndTerm_whenCalculate_thenReturnRepayment() {

		InterestCalculator cal = new CompoundInterestService();
		BigDecimal total = cal.repayment(new BigDecimal("1000"), new BigDecimal("0.07"), 36);
		assertThat(total, is(new BigDecimal("1232.93")));
	}

}
