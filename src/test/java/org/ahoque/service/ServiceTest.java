package org.ahoque.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.ahoque.Application;
import org.ahoque.domain.Offer;
import org.ahoque.repository.OfferRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ServiceTest {

	@Mock
	private OfferRepository repo;

	@InjectMocks
	private OfferService service;

	private Offer offer1, offer2, offer3;

	@Before
	public void setup() throws IOException {

		MockitoAnnotations.initMocks(this);

		List<Offer> offers = new ArrayList<>();
		offer1 = new Offer("Bob", new BigDecimal("0.075"), new BigDecimal("640"));
		offers.add(offer1);

		offer2 = new Offer("Hoque", new BigDecimal("0.065"), new BigDecimal("90"));
		offers.add(offer2);

		offer3 = new Offer("Jane", new BigDecimal("0.071"), new BigDecimal("800"));
		offers.add(offer3);

		Mockito.when(repo.findAll("market-data.csv")).thenReturn(offers);

	}

	@Test
	public void givenMarketDataAndLoanAmount_whenGetOffers_thenReturnListofOffers() throws IOException {

		List<Offer> offers = service.getOffers("market-data.csv", new BigDecimal("100"));
		assertTrue(offers.contains(offer1));
		assertTrue(offers.contains(offer3));
		offers.forEach(offer -> System.out.println(offer.getAvailable()));
	}

	@Test
	public void givenMarketDataAndLoanAmount_whenGetOffers_thenReturnOfferBestRate() throws IOException {

		List<Offer> offers = service.getBestRate("market-data.csv", new BigDecimal("100"));
		assertThat(offers.get(0), is(offer3));
	}

}
