Scenario:  Get best rate for the loan amount of £900
Given market data file market-data.csv
And loan amount 900
When the app is run
Then show Please enter loan amount as multiples of 100 and between £1000 and £15000

Scenario:  Get best rate for the loan amount of £16000
Given market data file market-data.csv
And loan amount 16000
When the app is run
Then show Please enter loan amount as multiples of 100 and between £1000 and £15000

Scenario:  Get best rate for the loan amount of £1643
Given market data file market-data.csv
And loan amount 1643
When the app is run
Then show Please enter loan amount as multiples of 100 and between £1000 and £15000

Scenario:  Get best rate for the loan amount of £4000
Given market data file market-data.csv
And loan amount 4000
When the app is run
Then print Sorry, currently there are no available offers

Scenario:  Get best rate for the loan amount of £1200
Given market data file market-data.csv
And loan amount 1200
When the app is run
Then the best rate should be 6.9%
Then Monthly repayment: £40.98
Then Total payment: £1475.10

Scenario:  Get best rate for the loan amount of £1600
Given market data file market-data.csv
And loan amount 1600
When the app is run
Then the best rate should be 7.5%
Then Monthly repayment: £55.62
Then Total payment: £2002.31

Scenario:  Get best rate for the loan amount of £1500
Given market data file market-data.csv
And loan amount 1500
When the app is run
Then the best rate should be 7.1%
Then Monthly repayment: £51.53
Then Total payment: £1854.91