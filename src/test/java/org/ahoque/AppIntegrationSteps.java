package org.ahoque;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.boot.SpringApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class AppIntegrationSteps {

	private String path;
	private String loanAmount;
	private ByteArrayOutputStream consoleStorage;
	PrintStream newConsole;

	@BeforeScenario
	public void setup() {
		consoleStorage = new ByteArrayOutputStream();
		newConsole = System.out;
	}

	@Given("market data file $path")
	public void givenTheMarketDataFilePath(String path) throws IOException {
		File file = new ClassPathResource(path).getFile();
		this.path = file.getAbsolutePath();
	}

	@Given("loan amount $amount")
	public void givenLoanAmount(String amount) {
		this.loanAmount = amount;
	}

	@When("the app is run")
	public void whenTheAppIsRun() throws Exception {
		System.setOut(new PrintStream(consoleStorage));
		SpringApplication.run(Application.class, path, loanAmount);
	}

	@Then("print $message")
	public void thenPrintMessage(String message) {
		String[] lines = consoleStorage.toString().split("\n");
		assertThat(lines[1], is(message));
	}

	@Then("show $message")
	public void thenShowMessage(String message) {
		String[] lines = consoleStorage.toString().split("\n");
		assertThat(lines[0], is(message));
	}

	@Then("the best rate should be $rate")
	public void thenTheBestRateShouldBe(String rate) {
		String[] lines = consoleStorage.toString().split("\n");
		assertTrue(lines[1].contains(rate));
	}

	@Then("Monthly repayment: $amount")
	public void thenMonthlyRepayment(String amount) {
		String[] lines = consoleStorage.toString().split("\n");
		assertTrue(lines[2].contains(amount));
	}

	@Then("Total payment: $amount")
	public void thenTotalPayment(String amount) {
		String[] lines = consoleStorage.toString().split("\n");
		assertTrue(lines[3].contains(amount));
	}

}
