package org.ahoque.repository;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.ahoque.domain.Offer;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

public class RepositoryTest {

	@Test
	public void givenCSVFile_whenFindAll_thenLoadLenderOffers() throws IOException {

		OfferRepository repo = new CsvRepository();

		File file = new ClassPathResource("market-data.csv").getFile();
		String path = file.getAbsolutePath();

		List<Offer> offers = repo.findAll(path);

		Offer offer = new Offer("Bob", new BigDecimal("0.075"), new BigDecimal("1640"));
		assertTrue(offers.contains(offer));

		Offer offer2 = new Offer("Angela", new BigDecimal("0.071"), new BigDecimal("1160"));
		assertTrue(offers.contains(offer2));

		offers.forEach(o -> System.out
				.println(o.getLender() + " " + o.getRate().toPlainString() + " " + o.getAvailable().toPlainString()));

	}

}
